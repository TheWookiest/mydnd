# MyDnD

It's my DnD with black jack and leveling.

JDK version - jdk-11.0.1

Angular version - 7.1.4

To run without IDE:

    backend: (from project source) - JAVA_HOME should be in class path. see https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/
        mvnw clean package
        java -jar target/dnd-1
    
    frontend: (from /frontend/mydnd)
        ng serve