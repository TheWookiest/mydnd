import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './component/login/login.component';
import { SignupComponent } from './component/signup/signup.component';
import { LenderPageComponent } from './component/lender-page/lender-page.component';
import { MyHeroesComponent } from './component/heroes/my-heroes/my-heroes.component';
import { TestComponent } from './component/test/test.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'signup', component: SignupComponent},
  {path: 'lender', component: LenderPageComponent},
  {path: 'my-heroes', component: MyHeroesComponent},
  {path: 'test', component: TestComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
