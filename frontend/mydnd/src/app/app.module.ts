import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { LoginComponent } from './component/login/login.component';
import { SignupComponent } from './component/signup/signup.component';
import { UserService } from './service/user.service';
import { LenderPageComponent } from './component/lender-page/lender-page.component';
import { MyHeroesComponent } from './component/heroes/my-heroes/my-heroes.component';
import { HeaderComponent } from './component/util/header/header.component';
import { FooterComponent } from './component/util/footer/footer.component';
import { AuthService } from './service/auth/auth.service';
import { AuthInterceptor } from './service/auth/interceptor/auth-interceptor.service';
import { LocalStorageService } from './service/auth/local-storage.service';
import { SessionService } from './service/auth/session.service';
import { TestComponent } from './component/test/test.component';
import { TestService } from './component/test/test.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    LenderPageComponent,
    MyHeroesComponent,
    HeaderComponent,
    FooterComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    UserService,
    AuthService,
    LocalStorageService,
    SessionService,
    TestService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
