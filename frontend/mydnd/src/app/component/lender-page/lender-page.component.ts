import { Component, OnInit } from '@angular/core';
import { RoomResponse } from 'src/app/model/response/room.response';

@Component({
  selector: 'app-lender-page',
  templateUrl: './lender-page.component.html',
  styleUrls: ['./lender-page.component.scss']
})
export class LenderPageComponent implements OnInit {

  rooms: Set<RoomResponse> = new Set<RoomResponse>();

  constructor() { }

  ngOnInit() {

    let room = new RoomResponse();
    room.id = 1;
    room.name = "Rooom" + room.id;
    room.master = "Wookie" + room.id;
    room.currentPlayers = 1;
    room.maxPlayers = 5;
    room.worldName = "WH";
    this.rooms.add(room);

    room = new RoomResponse();
    room.id = 2;
    room.name = "Rooom" + room.id;
    room.master = "Wookie" + room.id;
    room.currentPlayers = 2;
    room.maxPlayers = 5;
    room.worldName = "WH";
    this.rooms.add(room);

    room = new RoomResponse();
    room.id = 3;
    room.name = "Rooom" + room.id;
    room.master = "Wookie" + room.id;
    room.currentPlayers = 3;
    room.maxPlayers = 5;
    room.worldName = "WH";
    this.rooms.add(room);

    room = new RoomResponse();
    room.id = 4;
    room.name = "Rooom" + room.id;
    room.master = "Wookie" + room.id;
    room.currentPlayers = 4;
    room.maxPlayers = 5;
    room.worldName = "WH";
    this.rooms.add(room);
  }

}
