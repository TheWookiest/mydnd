import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth/auth.service';
import { AuthResponse } from 'src/app/model/response/auth.response';
import { SignupRequest } from 'src/app/model/request/signup.request';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  sendLoginRequest() {
    let signupRequest: SignupRequest = new SignupRequest();
    signupRequest.name = "test";
    signupRequest.password = "pass";


    this.authService.sendGetTokenRequest(signupRequest)
    .subscribe(
        (data: AuthResponse) => {
          console.log("aaa: " + data );
          window.location.href = "/lender";
        },
        err => console.log('HTTP Error:::', err)
     );
  }
}
