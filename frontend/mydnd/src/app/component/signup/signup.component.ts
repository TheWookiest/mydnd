import { Component, OnInit } from '@angular/core';
import { SignupRequest } from 'src/app/model/request/signup.request';
import { UserService } from 'src/app/service/user.service';
import { UserResponse } from 'src/app/model/response/user.response';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  // TODO: add real time password checking via server call. Add error notifications if username is already exists or password is not match.

  signupRequest: SignupRequest = new SignupRequest();
  repeatedPassword: string;

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.signupRequest = new SignupRequest();
  }

  sendSignupRequest() {
    if(this.repeatedPassword === this.signupRequest.password) {
      this.userService.sendSignupRequest(this.signupRequest)
      .subscribe((data: UserResponse) => {
        console.log("aaa: " + data.id );
        window.location.href = "/login";
      });
    }
  }

}
