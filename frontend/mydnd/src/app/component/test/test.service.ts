import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ConstantUtil } from 'src/app/util/constant.util';

@Injectable()
export class TestService {
    TEST_URL: string = "test/test";

    constructor(private http: HttpClient) {
    }

    sendTestRequest(): Observable<any> {
        console.log("Send request to " + ConstantUtil.BASE_URL + this.TEST_URL)
        return this.http.get<any>(
            ConstantUtil.BASE_URL + this.TEST_URL
        )
    }
}