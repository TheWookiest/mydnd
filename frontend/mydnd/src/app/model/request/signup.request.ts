export class SignupRequest {
    grant_type: string;
    name: string;
    password: string;
    refresh_token: string;
}