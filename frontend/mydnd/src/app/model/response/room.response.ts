export class RoomResponse {
    id: number;
    name: string;
    master: string;
    maxPlayers: number;
    currentPlayers: number;
    worldName: string;
}