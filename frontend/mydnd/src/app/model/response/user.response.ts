import { RoleResponse } from './role.response';

export interface UserResponse {
    id: number;
    name: string;
    roles: Set<RoleResponse>;
}