import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { ConstantUtil } from 'src/app/util/constant.util';
import { AuthResponse } from 'src/app/model/response/auth.response';
import { SignupRequest } from 'src/app/model/request/signup.request';

@Injectable()
export class AuthService {
    GET_TOKEN_URL: string = "oauth/token";
    REFRESH_TOKEN_URL: string = "oauth/token";
    CHECK_TOKEN_URL: string = "oauth/check_token"

    constructor(private http: HttpClient) {
    }

    sendGetTokenRequest(signupRequest: SignupRequest): Observable<AuthResponse> {
        if(!!signupRequest && !!signupRequest.name && !!signupRequest.password) {
            console.log("Send request to " + ConstantUtil.BASE_URL + this.GET_TOKEN_URL)

            let body = new URLSearchParams();
            body.set('username', signupRequest.name);
            body.set('password', signupRequest.password);
            body.set('grant_type', "password");

            console.log("b: " + body);

            return this.http.post<AuthResponse>(
                ConstantUtil.BASE_URL + this.GET_TOKEN_URL + "?" + body,
                null
            )
        } else {
            console.log("Error sendGetTokenRequest: field is empty " + signupRequest);
            return null;
        }
    }

    sendRefreshTokenRequest(signupRequest: SignupRequest): Observable<AuthResponse> {
        if(!!signupRequest && !!signupRequest.refresh_token) {
            console.log("Send request to " + ConstantUtil.BASE_URL + this.REFRESH_TOKEN_URL)

            signupRequest.grant_type = "refersh_token";

            return this.http.post<AuthResponse>(
                ConstantUtil.BASE_URL + this.REFRESH_TOKEN_URL,
                signupRequest
            )
        } else {
            console.log("Error sendGetTokenRequest: field is empty " + signupRequest);
            return null;
        }
    }
}