import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthResponse } from 'src/app/model/response/auth.response';
import { SessionService } from '../session.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private sessionService: SessionService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("auth intercepted")
        
        if (request.url.includes('oauth/')) {
            console.log("oauth header");
            request = request.clone({
                setHeaders: { 
                    Authorization: `Basic ZG5kLWNsaWVudDpkbmQtc2VjcmV0`
                }
            });
        } else if(!request.url.includes('/user/signup') && !request.url.includes('/user/check/')) { 
            // if request needs authorization
            console.log("needs auth. Token: " + this.sessionService.getAccessToken());

            let accessToken: string = this.sessionService.getAccessToken();
            if(!accessToken) {
                console.log("Token is null");
                // this.sessionService.get
            } 

            request = request.clone({
                setHeaders: { 
                    Authorization: `Bearer ` + accessToken
                }
            });
        }

        return next.handle(request);
    }
}