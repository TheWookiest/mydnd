import {Injectable} from '@angular/core';

@Injectable()
export class LocalStorageService {

 constructor() {}

 set(key: string, value: string) {
   sessionStorage.setItem(key, value);
 }

 get(key: string) {
   return sessionStorage.getItem(key);
 }

 clear() {
   sessionStorage.clear();
 }

 remove(key: string) {
   sessionStorage.removeItem(key);
 }
}