import {Inject, Injectable} from '@angular/core';
// import {LoggedInUserProfile, UserRole} from '../../../data/model/LoggedInUserProfile';
import {ActivatedRoute} from '@angular/router';
import {isNullOrUndefined} from 'util';
// import {JWT_HELPER_TOKEN} from '../../token/JWT_HELPER_TOKEN';
// import {JwtHelperService} from '@auth0/angular-jwt';
// import {LoginService} from '../../../data/LoginService';
import { LocalStorageService } from './local-storage.service';
import { UserResponse } from 'src/app/model/response/user.response';
import { AuthService } from './auth.service';
import { SignupRequest } from 'src/app/model/request/signup.request';
import { AuthResponse } from 'src/app/model/response/auth.response';

@Injectable()
export class SessionService {

  public productRequestCreated: boolean;
  public userResponse: UserResponse;
  private loggedInUserKey = 'signedUser';
  private tokenKey = 'accessToken';
  private refreshKey = 'refreshToken';

  constructor(private activatedRoute: ActivatedRoute,
              private localStorageService: LocalStorageService,
            //   @Inject(JWT_HELPER_TOKEN) private jwtHelper: JwtHelperService,
              private authService: AuthService) {

    console.log("Whaaaat?");            
    activatedRoute.queryParams.subscribe(params => {
      if (params['login']) {
        // this.userResponse = new UserResponse();
        this.userResponse.name = params['name'];
        this.userResponse.id = params['id'];

        console.log(JSON.stringify(this.userResponse));
        this.localStorageService.set(this.loggedInUserKey, JSON.stringify(this.userResponse));
      }
    });
  }

  getName(): string {
    if (this.getSignedUser()) {
      return this.capitalize(this.userResponse.name);
    } else {
      return '';
    }
  }

  getUserId(): number {
    if (this.getSignedUser()) {
      return this.userResponse.id;
    } else {
      return -1;
    }
  }

//   getUserEmail(): string {
//     if (this.getLoggedInUserProfile()) {
//       return this.loggedInUserProfile.userEmail;
//     } else {
//       return '';
//     }
//   }

  getSignedUser(): UserResponse {
    if (isNullOrUndefined(this.userResponse)) {
      if (this.localStorageService.get(this.loggedInUserKey)) {
        const userInfo = JSON.parse(this.localStorageService.get(this.loggedInUserKey));
        if (!isNullOrUndefined(userInfo)) {
          this.userResponse = userInfo;
        }
        return userInfo;
      }
    }
    return this.userResponse;
  }

  getAccessToken(): string {
    let token = this.localStorageService.get(this.tokenKey);
    if (!isNullOrUndefined(token)) {
        return token;
    } else {
      let signupRequest: SignupRequest = new SignupRequest();
      signupRequest.name = this.getName();
      // signupRequest.password = this.getPassword();
  
      this.authService.sendGetTokenRequest(signupRequest)
      .subscribe(
          (data: AuthResponse) => {
            console.log("aaa: " + data );
            window.location.href = "/lender";
          },
          err => console.log('HTTP Error:::', err)
      );

      return '';
    }
  }

  updateAccessToken(token: string) {
    this.localStorageService.set(this.tokenKey, token);
  }

  getRefreshToken(): string {
    let token = this.localStorageService.get(this.refreshKey);
    if (!isNullOrUndefined(token)) {
        return token;
    } else {
        return '';
    }
  }

  updateRefreshToken(token: string) {
    this.localStorageService.set(this.refreshKey, token);
  }

  clearSession() {
    this.userResponse = undefined;
    this.localStorageService.clear();
  }

//   isUserInAnyRole(...userRole: RoleResponse[]) {
//     return !!this.getSignedUser() && userRole.findIndex(role => role === this.getSignedUser().userRole) > -1;
//   }


  private capitalize(word: string): string {
    if (word) {
      return word.charAt(0).toUpperCase() + word.substring(1);
    }
  }

  // getTimeToExpiration(): number {
  //   const token = this.getMusSessionToken();
  //   if (!token || token === '') {
  //     return 0;
  //   }
  //   try {
  //     const user = this.jwtHelper.decodeToken(token);
  //     return  user.exp * 1000 - Date.now();
  //   } catch (e) {
  //     console.error('Unable to decode token', e);
  //     return 0;
  //   }
  // }
}