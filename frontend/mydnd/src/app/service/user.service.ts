import { SignupRequest } from '../model/request/signup.request';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { UserResponse } from '../model/response/user.response';
import { ConstantUtil } from '../util/constant.util';
import { Observable, from } from 'rxjs';

@Injectable()
export class UserService {
    SIGNUP_URL: string = "user/signup";

    constructor(private http: HttpClient) {
    }

    sendSignupRequest(signupRequest: SignupRequest): Observable<UserResponse> {
        console.log("Send request to " + ConstantUtil.BASE_URL + this.SIGNUP_URL)
        return this.http.post<UserResponse>(
            ConstantUtil.BASE_URL + this.SIGNUP_URL,
            signupRequest
        )
    }

}