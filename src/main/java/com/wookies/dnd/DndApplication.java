package com.wookies.dnd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:application-test.properties")
public class DndApplication {

    public static void main(String[] args) {
        SpringApplication.run(DndApplication.class, args);
    }

}

