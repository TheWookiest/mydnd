package com.wookies.dnd.config.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.token.TokenStore;

@Configuration
@EnableAuthorizationServer
public class OauthAuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    @Value("${oauth.realm}")
    private String oauthRealm;
    @Value("${oauth.client.name}")
    private String oauthClientName;
    @Value("${oauth.client.secret}")
    private String oauthClientSecret;
    @Value("${oauth.access.token.livetime}")
    private int accessTokenLivetime;
    @Value("${oauth.refresh.token.livetime}")
    private int refreshTokenLivetime;

    @Autowired
    private TokenStore tokenStore;
    @Autowired
    private UserApprovalHandler userApprovalHandler;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        System.out.println("1 : " + oauthClientName + oauthClientSecret);
        clients.inMemory()
                .withClient(oauthClientName)
                .secret(oauthClientSecret)
//                .authorizedGrantTypes("password", "authorization_code", "refresh_token", "implicit")
                .authorizedGrantTypes("password", "refresh_token")//, "client_credentials")
                .authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT")
                .scopes("read", "write", "trust")
                .accessTokenValiditySeconds(accessTokenLivetime). //3600
                refreshTokenValiditySeconds(refreshTokenLivetime); //604800-week
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .tokenStore(tokenStore)//.accessTokenConverter(accessTokenConverter())
                .reuseRefreshTokens(false) // Make refresh_token re-grant after refreshing an access token
                .userApprovalHandler(userApprovalHandler)
                .authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.allowFormAuthenticationForClients();
        oauthServer.checkTokenAccess("permitAll()");
        oauthServer.realm(oauthRealm + "/client");
    }
}
