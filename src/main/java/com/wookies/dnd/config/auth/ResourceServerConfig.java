package com.wookies.dnd.config.auth;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableResourceServer
@EnableWebSecurity
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "mydnd_rest_api";

    @Value("${oauth.realm}")
    private String oauthRealm;
    @Value("${oauth.client.name}")
    private String oauthClientName;
    @Value("${oauth.client.secret}")
    private String oauthClientSecret;


    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
            .anonymous().and()
                .csrf().disable()
                .cors().disable()
            .authorizeRequests()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/oauth/**").permitAll()
                .antMatchers("/test/no-auth/**", "/user/signup", "/user/check/**").permitAll()
                .antMatchers("/**").authenticated();
//            .and()
//                .exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }
}

