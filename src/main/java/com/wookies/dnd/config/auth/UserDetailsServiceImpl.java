package com.wookies.dnd.config.auth;

import com.wookies.dnd.model.entity.RoleEntity;
import com.wookies.dnd.model.entity.UserEntity;
import com.wookies.dnd.repository.UserRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        UserEntity credentials = userRepository.findByName(login).orElseThrow(
                () -> new UsernameNotFoundException("Bad credentials")
        );

        return new org.springframework.security.core.userdetails.User(
                credentials.getName(),
                credentials.getPassword(),
                true, true, true, true, getGrantedAuthorities(credentials));

    }

    private List<GrantedAuthority> getGrantedAuthorities(UserEntity credentials) {
        List<GrantedAuthority> authorities = new ArrayList<>();

        for(RoleEntity role : credentials.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return authorities;
    }
}

