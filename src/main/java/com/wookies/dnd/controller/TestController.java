package com.wookies.dnd.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/test")
    public void test(@AuthenticationPrincipal Principal principal) {
        System.out.println("PRINCIPAL : " + principal);
    }

    @GetMapping("/no-auth/test-string/{str}")
    public void testString(@PathVariable String str) {
        System.out.println("DA STRING : " + str);
    }
}
