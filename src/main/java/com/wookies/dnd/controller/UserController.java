package com.wookies.dnd.controller;

import com.wookies.dnd.model.request.SignupRequest;
import com.wookies.dnd.model.response.UserResponse;
import com.wookies.dnd.model.response.transformer.impl.UserEntityToUserResponseTransformer;
import com.wookies.dnd.exceptions.UserAlreadyExistsException;
import com.wookies.dnd.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/signup")
    public ResponseEntity<UserResponse> signup(@RequestBody SignupRequest signupRequest) {
        System.out.println("requ: " + signupRequest);
        try {
            return new ResponseEntity<>(
                    UserEntityToUserResponseTransformer.transform(userService.signup(signupRequest)),
                    HttpStatus.CREATED
            );
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/check/{name}")
    public ResponseEntity checkUserExistence(@PathVariable String name) {
        try {
            userService.checkNameExistence(name);
            return new ResponseEntity(HttpStatus.OK);
        } catch (UserAlreadyExistsException ex) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }
}
