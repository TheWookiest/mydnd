package com.wookies.dnd.exceptions;

public class UserAlreadyExistsException extends Exception {

    public UserAlreadyExistsException(String message) {
        super(message);
    }

    public UserAlreadyExistsException(Integer id) {
        super("User with id={" + id + "} is already exists");
    }
}
