package com.wookies.dnd.model.entity;

import com.wookies.dnd.model.entity.builder.RoleEntityBuilder;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Role")
public class RoleEntity {
    public static final RoleEntity ADMIN = new RoleEntityBuilder().setId(1L).setName("ADMIN").build();
    public static final RoleEntity USER = new RoleEntityBuilder().setId(2L).setName("USER").build();
    public static final RoleEntity MASTER = new RoleEntityBuilder().setId(3L).setName("MASTER").build();

    @Id
    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoleEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleEntity that = (RoleEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
