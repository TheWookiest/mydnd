package com.wookies.dnd.model.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "Room")
public class RoomEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name = "max_players")
    private Integer maxPlayers;

    @OneToMany(mappedBy = "id")
    private Collection<HeroEntity> heroes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(Integer maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public Collection<HeroEntity> getHeroes() {
        return heroes;
    }

    public void setHeroes(Collection<HeroEntity> heroes) {
        this.heroes = heroes;
    }

    @Override
    public String toString() {
        return "RoomEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", maxPlayers=" + maxPlayers +
                ", heroes=" + heroes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomEntity that = (RoomEntity) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(maxPlayers, that.maxPlayers) &&
                Objects.equals(heroes, that.heroes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, maxPlayers, heroes);
    }
}
