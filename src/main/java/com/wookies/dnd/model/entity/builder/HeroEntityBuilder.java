package com.wookies.dnd.model.entity.builder;

import com.wookies.dnd.model.entity.HeroEntity;
import com.wookies.dnd.model.entity.RoomEntity;
import com.wookies.dnd.model.entity.UserEntity;

public class HeroEntityBuilder {

    private HeroEntity heroEntity = new HeroEntity();

    public HeroEntityBuilder setId(Long id) {
        this.heroEntity.setId(id);
        return this;
    }

    public HeroEntityBuilder setName(String name) {
        this.heroEntity.setName(name);
        return this;
    }

    public HeroEntityBuilder setUser(UserEntity user) {
        this.heroEntity.setUser(user);
        return this;
    }

    public HeroEntityBuilder setRoom(RoomEntity room) {
        this.heroEntity.setRoom(room);
        return this;
    }

    public HeroEntity build() {
        return this.heroEntity;
    }
}
