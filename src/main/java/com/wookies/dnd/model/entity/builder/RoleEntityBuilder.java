package com.wookies.dnd.model.entity.builder;

import com.wookies.dnd.model.entity.RoleEntity;

public class RoleEntityBuilder {

    private RoleEntity roleEntity = new RoleEntity();

    public RoleEntityBuilder setId(Long id) {
        this.roleEntity.setId(id);
        return this;
    }

    public RoleEntityBuilder setName(String name) {
        this.roleEntity.setName(name);
        return this;
    }

    public RoleEntity build() {
        return roleEntity;
    }
}
