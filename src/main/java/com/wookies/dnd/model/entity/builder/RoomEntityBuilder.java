package com.wookies.dnd.model.entity.builder;

import com.wookies.dnd.model.entity.HeroEntity;
import com.wookies.dnd.model.entity.RoomEntity;

import java.util.Collection;

public class RoomEntityBuilder {

    private RoomEntity roomEntity = new RoomEntity();

    public RoomEntityBuilder setId(Long id) {
        this.roomEntity.setId(id);
        return this;
    }

    public RoomEntityBuilder setName(String name) {
        this.roomEntity.setName(name);
        return this;
    }

    public RoomEntityBuilder setMaxPlayers(Integer maxPlayers) {
        this.roomEntity.setMaxPlayers(maxPlayers);
        return this;
    }

    public RoomEntityBuilder setHeroes(Collection<HeroEntity> heroes) {
        this.roomEntity.setHeroes(heroes);
        return this;
    }

    public RoomEntityBuilder addHero(HeroEntity heroEntity) {
        this.roomEntity.getHeroes().add(heroEntity);
        return this;
    }

    public RoomEntity build() {
        return this.roomEntity;
    }
}
