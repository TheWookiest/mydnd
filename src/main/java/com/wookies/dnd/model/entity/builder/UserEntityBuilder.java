package com.wookies.dnd.model.entity.builder;

import com.wookies.dnd.model.entity.HeroEntity;
import com.wookies.dnd.model.entity.RoleEntity;
import com.wookies.dnd.model.entity.UserEntity;

import java.util.*;

public class UserEntityBuilder {
    private UserEntity userEntity = new UserEntity();

    public UserEntityBuilder setId(Long id) {
        this.userEntity.setId(id);
        return this;
    }

    public UserEntityBuilder setName(String name) {
        this.userEntity.setName(name);
        return this;
    }

    public UserEntityBuilder setPassword(String password) {
        this.userEntity.setPassword(password);
        return this;
    }

    public UserEntityBuilder setRoles(Set<RoleEntity> roles) {
        this.userEntity.setRoles(roles);
        return this;
    }

    public UserEntityBuilder addRole(RoleEntity role) {
        this.userEntity.getRoles().add(role);
        return this;
    }


    public UserEntityBuilder setHeroes(Collection<HeroEntity> heroes) {
        this.userEntity.setHeroes(heroes);
        return this;
    }

    public UserEntityBuilder addHero(HeroEntity heroEntity) {
        this.userEntity.getHeroes().add(heroEntity);
        return this;
    }

    public UserEntity build() {
        return this.userEntity;
    }
}
