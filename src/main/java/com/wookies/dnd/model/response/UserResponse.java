package com.wookies.dnd.model.response;

import com.wookies.dnd.model.entity.HeroEntity;

import java.util.*;

public class UserResponse {
    private Long id;
    private String name;
    private List<RoleResponse> roles = new ArrayList<>(2);
    private Collection<HeroEntity> heroes = new ArrayList<>(); //TODO: change to HeroResponse

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RoleResponse> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleResponse> roles) {
        this.roles = roles;
    }

    public Collection<HeroEntity> getHeroes() {
        return heroes;
    }

    public void setHeroes(Collection<HeroEntity> heroes) {
        this.heroes = heroes;
    }
}
