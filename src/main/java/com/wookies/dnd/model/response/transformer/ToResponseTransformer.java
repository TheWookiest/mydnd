package com.wookies.dnd.model.response.transformer;

import com.mysql.cj.exceptions.WrongArgumentException;

public interface ToResponseTransformer<E, R> {

    R transform(E toTransform) throws WrongArgumentException;

}
