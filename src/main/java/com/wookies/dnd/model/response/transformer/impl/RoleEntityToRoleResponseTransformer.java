package com.wookies.dnd.model.response.transformer.impl;

import com.mysql.cj.exceptions.WrongArgumentException;
import com.wookies.dnd.model.response.RoleResponse;
import com.wookies.dnd.model.entity.RoleEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RoleEntityToRoleResponseTransformer {//implements ToResponseTransformer<RoleEntity, RoleResponse> {

//    @Override
    public static RoleResponse transform(RoleEntity toTransform) throws WrongArgumentException {

        if(toTransform == null) {
            throw new WrongArgumentException("Cannot transform {" + toTransform + "} to RoleResponse");
        }

        RoleResponse response = new RoleResponse();

        response.setId(toTransform.getId());
        response.setName(toTransform.getName());

        return response;
    }

    public static List<RoleResponse> transform(Collection<RoleEntity> toTransform) {
        List<RoleResponse> response = new ArrayList<>(toTransform.size());

        for(RoleEntity e : toTransform) {
            response.add(transform(e));
        }

        return response;
    }

}
