package com.wookies.dnd.model.response.transformer.impl;

import com.mysql.cj.exceptions.WrongArgumentException;
import com.wookies.dnd.model.response.UserResponse;
import com.wookies.dnd.model.entity.UserEntity;

public class UserEntityToUserResponseTransformer {//implements ToResponseTransformer<UserEntity, UserResponse> {

//    @Override
    public static UserResponse transform(UserEntity toTransform) throws WrongArgumentException {

        if(toTransform == null) {
            throw new WrongArgumentException("Cannot transform {" + toTransform + "} to UserResponse");
        }

        UserResponse response = new UserResponse();

        response.setId(toTransform.getId());
        response.setName(toTransform.getName());
        response.setRoles(RoleEntityToRoleResponseTransformer.transform(toTransform.getRoles()));
        response.setHeroes(toTransform.getHeroes());

        return response;
    }
}
