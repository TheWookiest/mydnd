package com.wookies.dnd.repository;

import com.wookies.dnd.model.entity.HeroEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeroRepository extends PagingAndSortingRepository<HeroEntity, Long> {
}
