package com.wookies.dnd.repository;

import com.wookies.dnd.model.entity.RoleEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<RoleEntity, Long> {
}
