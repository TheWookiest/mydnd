package com.wookies.dnd.repository;

import com.wookies.dnd.model.entity.RoomEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends PagingAndSortingRepository<RoomEntity, Long> {
}
