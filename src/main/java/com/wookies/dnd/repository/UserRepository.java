package com.wookies.dnd.repository;

import com.wookies.dnd.model.entity.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

    public Optional<UserEntity> findByName(String name);

}
