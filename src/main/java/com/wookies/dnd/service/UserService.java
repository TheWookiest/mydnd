package com.wookies.dnd.service;

import com.wookies.dnd.model.request.SignupRequest;
import com.wookies.dnd.exceptions.UserAlreadyExistsException;
import com.wookies.dnd.model.entity.UserEntity;
import org.springframework.transaction.annotation.Transactional;


public interface UserService {

    @Transactional
    UserEntity signup(SignupRequest signupRequest) throws UserAlreadyExistsException;

    @Transactional(readOnly = true)
    void checkNameExistence(String name) throws UserAlreadyExistsException;
}
