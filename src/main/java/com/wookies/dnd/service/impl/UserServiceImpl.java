package com.wookies.dnd.service.impl;

import com.mysql.cj.exceptions.WrongArgumentException;
import com.wookies.dnd.model.request.SignupRequest;
import com.wookies.dnd.exceptions.UserAlreadyExistsException;
import com.wookies.dnd.model.entity.RoleEntity;
import com.wookies.dnd.model.entity.UserEntity;
import com.wookies.dnd.model.entity.builder.UserEntityBuilder;
import com.wookies.dnd.repository.RoleRepository;
import com.wookies.dnd.repository.UserRepository;
import com.wookies.dnd.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserEntity signup(SignupRequest signupRequest) throws UserAlreadyExistsException, WrongArgumentException {
        System.out.println("ATATAT: " + roleRepository.findAll());
//        if (signupRequest == null)
//            throw new WrongArgumentException("UserServiceImpl.signup(null)");
        if(userRepository.findByName(signupRequest.getName()).isPresent()) {
            throw new UserAlreadyExistsException("User with name: {" + signupRequest.getName() + "} is already exists");
        }

        return userRepository.save(new UserEntityBuilder()
                .setName(signupRequest.getName())
                .setPassword(passwordEncoder.encode(signupRequest.getPassword()))
                .addRole(RoleEntity.USER)
                .build());
    }

    @Override
    public void checkNameExistence(String name) throws UserAlreadyExistsException {
        if(userRepository.findByName(name).isPresent()) {
            throw new UserAlreadyExistsException("User with name: {" + name + "} is already exists");
        }
    }
}
