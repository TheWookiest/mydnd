package com.wookies.dnd.repository;

import com.wookies.dnd.model.entity.RoleEntity;
import com.wookies.dnd.model.entity.UserEntity;
import com.wookies.dnd.model.entity.builder.RoleEntityBuilder;
import com.wookies.dnd.model.entity.builder.UserEntityBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@TestPropertySource("classpath:/application-test.properties")
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testRoleEagerLoad() {
        RoleEntity testRole = new RoleEntityBuilder()
                .setId(1L)
                .setName("test role")
                .build();

        UserEntity user = userRepository.save(new UserEntityBuilder()
                .setName("admin")
                .setPassword("pass")
                .addRole(testRole)
                .build());

        assertTrue(userRepository.findById(user.getId()).isPresent());
        assertTrue(userRepository.findById(user.getId()).get().getRoles().contains(testRole));
    }

    @Test
    public void testFindByName() {
        RoleEntity testRole = new RoleEntityBuilder()
                .setId(1L)
                .setName("test role")
                .build();

        UserEntity user = userRepository.save(new UserEntityBuilder()
                .setName("admin")
                .setPassword("pass")
                .addRole(testRole)
                .build());

        assertTrue(userRepository.findById(user.getId()).isPresent());
        assertTrue(userRepository.findByName(user.getName()).get().getRoles().contains(testRole));
    }
}