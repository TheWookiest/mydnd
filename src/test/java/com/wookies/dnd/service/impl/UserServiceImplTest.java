package com.wookies.dnd.service.impl;

import com.wookies.dnd.exceptions.UserAlreadyExistsException;
import com.wookies.dnd.model.entity.builder.UserEntityBuilder;
import com.wookies.dnd.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@TestPropertySource("classpath:/application-test.properties")
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Autowired
    private UserServiceImpl userService;

    @Test(expected = UserAlreadyExistsException.class)
    public void testCheckUserExistence_ifUserExists() throws UserAlreadyExistsException {

        when(userRepository.findByName("test")).thenReturn(Optional.of(
                new UserEntityBuilder()
                .build()
        ));

        userService.checkNameExistence("test");

    }

    @Test
    public void testCheckUserExistence_ifUserDoesNotExists() throws UserAlreadyExistsException {

        userService.checkNameExistence("test");

    }

}